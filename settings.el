(setq user-full-name "Pathompong Kwangtong")
(setq user-mail-address "foxmean@protonmail.com")

(require 'package)
(setq package-archives nil)
(setq package-enable-at-startup nil)
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/lisp/")

(require 'use-package)

(require 'which-key)
(which-key-mode)

(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(require 'ivy-bibtex)
(global-set-key (kbd "C-c C-r") 'ivy-bibtex)

(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(global-set-key (kbd "C-x o") 'ace-window)

(setq make-backup-files nil)
(setq auto-save-default nil)

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch)

(global-magit-file-mode)

(require 'git-timemachine)

(require 'diff-hl)
(global-diff-hl-mode)
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

(setq make-backup-files nil)

(require 'emms)
(require 'emms-player-mpv)
(add-to-list 'emms-player-list 'emms-player-mpv)

(require 'pdf-tools)
(pdf-tools-install)

(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
                                           :height 1.0))
(add-hook 'nov-mode-hook 'my-nov-font-setup)
(setq nov-text-width 80)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(add-to-list 'auto-mode-alist '("\\.\\(cbr\\)\\'" . archive-mode))

(global-set-key (kbd "C-c c") 'clone-indirect-buffer-other-window)

(global-flycheck-mode)
(add-hook 'haskell-mode-hook #'flycheck-haskell-setup)
(eval-after-load 'flycheck
  '(require 'flycheck-ledger))
(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(add-hook 'after-init-hook 'global-company-mode)

(require 'yasnippet)
(yas-global-mode 1)

(require 'fcitx)

(fcitx-aggressive-setup)

(require 'ac-geiser)
(add-hook 'geiser-mode-hook 'ac-geiser-setup)
(add-hook 'geiser-repl-mode-hook 'ac-geiser-setup)
(eval-after-load "auto-complete"
  '(add-to-list 'ac-modes 'geiser-repl-mode))
(require 'scribble-mode)
(add-hook 'scribble-mode-hook #'geiser-mode)

(require 'ledger-mode)
(setq ledger-clear-whole-transactions 1)
(add-to-list 'auto-mode-alist '("\\.dat\\'" . ledger-mode))

(require 'org-ref)
(setq org-ref-default-bibliography "~/zotero.bib")

(defun org-export-latex-no-toc (depth)
    (when depth
      (format "%% Org-mode is exporting headings to %s levels.\n"
              depth)))
(setq org-export-latex-format-toc-function 'org-export-latex-no-toc)

(add-to-list 'org-latex-classes
	'("apa6"
                 "\\documentclass{apa6}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(toggle-frame-maximized)
(display-time-mode 1)
(setq frame-title-format (concat "Foxmean's Emacs"))
(global-emojify-mode)
(global-prettify-symbols-mode 1) ; Display “lambda” as “λ”
(global-set-key (kbd "C-x C-l") 'display-line-numbers-mode)
(menu-bar-mode 1)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(setq inhibit-startup-screen t)
(global-subword-mode 1) ; Move by SubWord
(show-paren-mode 1) ; Turn on bracket match highlight
(save-place-mode 1) ; Remember cursor position
(defalias 'yes-or-no-p 'y-or-n-p)
(require 'hide-mode-line) ; For hide mode-line when nedd
(require 'fill-column-indicator)
(require 'all-the-icons) ; Download icons

(set-frame-font "Hack-10")
(set-fontset-font t 'thai "TlwgTypist-12")

(require 'spaceline-config)
(spaceline-emacs-theme)
(require 'anzu)
(setq anzu-cons-mode-line-p nil)
(global-anzu-mode +1)

(load-theme 'dracula t)

(require 'org)
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(setq org-confirm-babel-evaluate nil)

(setq org-src-fontify-natively t)

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c r") 'org-capture)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c L") 'org-insert-link-global)
(global-set-key (kbd "C-c O") 'org-open-at-point-global)
(global-set-key (kbd "<f9> <f9>") 'org-agenda-list)
(global-set-key (kbd "<f9> <f8>") (lambda () (interactive) (org-capture nil "r")))

(setq org-capture-templates '(
  ("a" "Appointment" entry (file+headline "~/journals/organizer.org" "Appointment")
    "*** %? <%<%Y-%m-%d %a>>\n"
    :empty-lines 0)
  ("f" "Festival" entry (file+headline "~/journals/organizer.org" "Festival")
    "*** %? <%<%Y-%m-%d %a>>\n"
    :empty-lines 0)
    ))

(find-file "~/journals/organizer.org")
(org-agenda nil "a")
